
Now, after some motivational remarks on the essence of groups, we can proceed with declaration of the more accurate results.
We'll be very formal here because it is the basis of all the further considerations, and start with the definition(s).
Although explanations are going to be, conversely, vary informal.

 
The two definitions and uniqueness
================================

> **Definitino:** a group is
> 
> - a set (or class, or type) $A$ (_carrier_)
> - equipped with a binary operation $m: A\times A\rightarrow A$
> - such that for any $a, b, c: A$, $$m(a, m(b, c))=m(m(a, b), c)$$ (_associativity_)
> - distinguished elements $e_0, e_1: A$
> - such that for any $a: A$, $$m( e_0, a ) = a = m( a, e_1 )$$ (_identity(-ies)_)
> - and two functions $r_0, r_1: A\rightarrow A$
> - such that for any $a: A$, $$m( r_0(a), a ) = e_0$$ $$m( a, r_1(a) ) = e_1$$ (_inverse(s)_)

From this definition we can derive:

First, that there is no other identity(-ies) than $e_0, e_1$, and these two are, in fact, the same element, call it just $e$, i.e.:

> For any $e'_0, e'_1: A$, such that for any $a: A$,
> 
> $$m( e'_0, a ) = a = m( a, e'_1 )$$
> 
> We have: $e'_0 = e_1$, $e'_1 = e_0$ and, moreover, $e_0 = e_1 =: e$.
> 
> **Proof:** for the casees $a = e_0$, $a = e_1$, $a = e'_0$, $a = e'_1$,
> 
> $$e_0' = m( e'_0, e_1 ) = e_1$$
> 
> $$e_1' = m( e_0, e'_1 ) = e_0$$
> 
> $$e_0 = m( e_0, e_1 ) = e_1$$

Second, that there is no other inverse(s) than $r_0$, $r_1$, and that these two are, in fact, the same function, call it just $r$, i.e.:

> For any $r'_0, r'_1: A \rightarrow A$, such that for any $a: A$,
> 
> $$m( r'_0(a), a ) = e_0$$
> 
> $$m( a, r'_1(a) ) = e_1$$
> 
> We have: $r'_0 = r_1$ and $r'_1 = r_0$, and, moreover, $r_0 = r_1 =: r$.
> 
> **Proof:** multiplying an element $a$ by both inverses, from each side, then using associativity:
> 
> 
> $$r'_0(a)= m( r'_0(a), e_1 ) = m( r'_0(a), m( a, r_1(a) ) ) = m( m( r'_0(a), a ), r_1(a) ) = m( e_0, r_1(a) ) = r_1(a)$$
> 
> $$r'_1(a) = m( e_0, r'_1(a) ) = m( m( r_0(a), a), r'_1(a) ) = m( r_0(a), m( a, r'_1(a) ) ) = m( r_0(a), e_1 ) = r_0(a)$$
> 
> $$r_0(a) = m( r_0(a), e_1 ) = m( r_0(a), m( a, r_1(a) ) ) = m( m( r_0(a), a ), r_1(a) ) = m( e_0, r_1(a) ) = r_1(a)$$

It means that in order to specify a group, it is enough to specify only the group operation: identity and inverse, if exist, have no uncertainty.
And it is not even necessary to ensure that these given identity(-ies) and inverse(s) are the same, they just have to satisfy the required properties.
Moreover, it is also possible to consider an algebraic structure with only half of the requirements on identity and inverse (only left or only right).
But then they may be not unique for the same group operation.

So, a group may be denoted by the same symbol as the carrier $A$, if it is clear with respect to which operation it forms a group.
Or, if not, then by the (formal) pair $(A, m)$, to mention the operation explicitly.
As long as we're not concerned about constructive aspects of the structures, we are enough to know that the identity and inverse are just somehow defined and have the necessary properties.

A note: if there are several operation in use, then there are several units and inverses, for each operation respectively, and the notation for the units and inverses should be different too and suggest to which group operation each of them is related.

--------

As mentioned above, there is an equivalent, more "raw", definition.

> **Definitino:** a group is
> 
> - a set (or class, or type) $A$
> - equipped with a binary operation $m: A\times A\rightarrow A$
> - such that for any $a, b, c: A$, $$m(a, m(b, c))=m(m(a, b), c)$$
> - two binary operations $d_0, d_1: A\times A\rightarrow A$
> - such that  for any $a, b: A$, $$m( d_0(b, a), a ) = b = m( a, d_1(b, a) )$$
> - an element $f: A$

And it is an interesting exercise, to prove their equivalence.
In one direction it is straightforward, just let

> $d_0(b, a) := m( b, r(a) )$ and $d_1(b, a) := m( r(a), b )$
> 
> $f := e$

In the other direction it is a bit trickier:

First, we prove that there is an identity element with desirable properties.

> Define two (left and right) units for every element $a$
> 
> $e_{0,a} := d_0(a, a)$ and $e_{1,a} := d_1(a, a)$
> 
> $$m( e_{0,a}, a ) = a = m( a, e_{1,a} )$$
> 
> Then apply the divisions for $a$ and some otner element $b$
> 
> $$b = m( d_0(b, a), a ) = m( d_0(b, a), m( a, e_{1,a} ) ) = m( m( d_0(b, a), a ), e_{1,a} ) = m( b, e_{1,a} )$$
> 
> $$b = m( a, d_1(b, a) ) = m( m( e_{0,a}, a ), d_1(b, a) ) = m( e_{0,a}, m( a, d_1(b, a) ) ) = m( e_{0,a}, b )$$
> 
> $$m( e_{0,a}, b ) = b = m( b, e_{1,a} )$$
> 
> So, $e_{0,a}$ and $e_{1,a}$, for any $a: A$, serve as a left and right unit (not only for $a$ but also for every $b: A$).
> Using the last axiom, we know the carrier is not empty, so the identity element(s) really exist(s):
> 
> $e_0 := e_{0,f} = d_0(f, f)$ and $e_1 := e_{1,f} = d_1(f, f)$

Second, we prove that there is an inverse

> Define two (left and right) inverses, using given divisions and the unit, which we know exists
> 
> $r_0(a) := d_0(e, a)$ and $r_1(a) := d_1(e, a)$
> $m( r_0(a), a ) = e = m( a, r_1(a) )$

So, these two definitions are indeed equivalent.


> Written with [StackEdit](https://stackedit.io/).