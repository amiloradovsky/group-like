
Now we introduce powers, and orders, of group elements (and finally switch to the more familiar notation(s), resembling the operations for natural numbers).

Powers are interesting mainly as an example of fusion of analytically and synthetically defined structures (integer numbers and groups): former are defined as logical constructions, latter are defined by declaration of properties.

More on the difference between analytic and synthetic:

| analytic       | synthetic
| -
| concrete       | abstract
| construction   | description
| implementation | interface
| structure      | signature
| term (element) | type (set?)
| proof/witness  | statement/claim
| etc.           | etc.

(Sure, it is a relative distinction.)

Also, sets of all powers of an elemenent(s) are a canonical example of non-trivial subgroups of a group (i.e. other than the unit alone and the group itself), and of an Abelian group (cyclic group).
And give rise to introduction of the concept of modules and vector spaces (from algebraic perspective, not geometric).

Integer power of a group element
----------------------------------------------------------------

We define the integer powers, as usual, recursively.
Adopting, by the way, juxtaposition notation ($ab$ instead of $m( a, b )$).

> **Definition:** for a group $A$, integer power of an element is an operation
> 
> $$A\times\mathbb{Z}\rightarrow A$$
>  
>  such that for any $a: A$, $$a^{n+1} := a^n a^1$$ $$a^1 := a$$

This operation exists because it is clear how the value may be computed for any arguments from the given range(s):

For positive powers: from the definition.
For zero power:

> $$a^0 = e$$
> 
> **Because**, for $n = 0$,
> 
> $$a = a^1 = a^{n+1} = a^n a^1 = a^0 a$$
> 
> So, $a^0$ is a left unit, and since there also _exists_ a right unit, then they are equal.

For $-1$-th power:

> $$a^{-1} = r(a)$$
> 
> **Because**, for $n + 1 = 0$,
>
> $$e = a^0 = a^{n+1} = a^n a^1 = a^{-1} a$$
> 
> So $a^{-1}$ is a left inverse for any $a$, and since there also _exists_ a right inverse $r(a)$, they are equal.

For further negative:

> $$a^{n-1} = a^n a^{-1}$$
> 
> (by replacement of $n$ in $a^{n+1} = a^n a$ with $n-1$ and multiplying the expression by $a^{-1}$ from right)

So, from now on, we can use $a^0$ for $e$ and $a^{-1}$ for $r(a)$, if it looks prettier.

--------

From this we can derive some of the familiar properties of powers of positive rational numbers.
(call it "mixed" distributivity, actually just one of two, and associativity respectively).

> For all $n, m: \mathbb{Z}$ $$a^{n+m} = a^n a^m$$ $$(a^n)^m = a^{nm}$$
> 
> **Proof:** for each fixed $n$, by induction on $m$:
> 
> Base case, let it be $m = 0$ $$a^{n+0} = a^n = a^n a^0$$
> 
> Inductive case(s):
> By the assumption, $a^{n+m} = a^n a^m$, and using respective inductive formulas and associativity:
> 
> For positive $m$
> 
> $$a^{n+(m+1)} = a^{(n+m)+1} = a^{n+m} a^1 = (a^n a^m) a^1 = a^n (a^m a^1) = a^n a^{m+1}$$
> 
> I.e., $$a^{n+m} = a^n a^m \implies a^{n+(m+1)} = a^n a^{m+1}$$
> 
> For negative $m$
> 
> $$a^{n+(m-1)} = a^{(n+m)-1} = a^{n+m} a^{-1} = (a^n a^m) a^{-1} = a^n (a^m a^{-1}) = a^n a^{m-1}$$
> 
> I.e., $$a^{n+m} = a^n a^m \implies a^{n+(m-1)} = a^n a^{m-1}$$
> 
> **Proof:** by the same induction (on the same variables):
> 
> Base case, again $m = 0$ $$(a^n)^0 = a^0 = a^{n 0}$$
> 
> Inductive case(s):
> Assume, $(a^n)^m = a^{nm}$, then:
> 
> For positive $m$
> 
> $$(a^n)^{m+1} = (a^n)^m (a^n)^1 = a^{nm} a^n = a^{nm+n} = a^{n(m+1)}$$
> 
> I.e., $$(a^n)^m = a^{nm} \implies (a^n)^{m+1} = a^{n(m+1)}$$
> 
> For negative $m$
> 
> $$(a^n)^{m-1} = (a^n)^m (a^n)^{-1} = a^{nm} a^{-n} = a^{nm-n} = a^{n(m-1)}$$
> 
> I.e., $$(a^n)^m = a^{nm} \implies (a^n)^{m-1} = a^{n(m-1)}$$
> 
> So the positive and negative inductive cases are almost identical, except one wrinkle:
> We used the facts that $(a^n)^1 = a^n$ and $(a^n)^{-1} = a^{-n}$.
> While the former is held directly by the definition, the latter is because
> 
> $$e = a^0 = a^{n+(-n)} = a^n a^{-n} \implies a^{-n} = r(a^n) = (a^n)^{-1}$$

Corollary:

> For all $n, m: \mathbb{Z}$ $$a^n a^m = a^m a^n$$ $$(a^n)^m = (a^m)^n$$

These results would also hold for monoids and semigroups, if this operation (power) would be properly defined.
Requirement that that recursive formula must hold for any integer power might lead to non-existence of this operation itself, if the structure is not a group:

- a monoid might not contain an elements to be the results of negative powers
- a semigroup might not even contain an element to be the zero power

The second "mixed" distributivity
----------------------------------------------------------------

Those are properties involving just a single element of a (semi)group.
Here are some other, involving more than one element.

First, a property connecting multiplication and inverse: distributivity of the inverse:

> For any $a, b: A$, $r( m( a, b ) ) = m( r(b), r(a) )$
> 
> **Proof:** multiply the expression
> 
> $$m( m( r( m( a, b) ), a ), b) = m( r( m( a, b) ), m( a, b) ) = e$$
> 
> from right, first by $r(b)$, then by $r(a)$
> 
> $$r( m( a, b) ) = m( r(b), r(a) )$$

Also, note that when the rule is applied, the inverses in the product stand in reverse order compared to the original product: it is a consequence of general non-commutativity of the group operation.
It is a good habit to process an expressions using as few rules as possible, only necessary, as if there were no other: in particular, even if the group operation is commutative, it is considerably bad idea to change the order of multipliers/additives without necessity, or leave them in the same order after applying this distributivity rule for inverse.
The reason for that is in the ease of generalization of an expression to a structures with fewer properties, these generalizations are so more obvious.

--------

commuting pairs of elements and the general distributivity

> But $$(a b)^n = b^n a^n$$ is true for only $n=-1$, or if the group operation is commutative.

Abelian groups and embedding
----------------------------------------------------------------

Unfortunately, it is not so easy (if possible) to construct an _intuitive_ example of a non-commutative (non-Abelian) group(oid).
And commutativity allows to embed every (commutative) monoid into a (commutative) group, by considering formal pairs of the elements of the monoid and partitioning them into classes, and defining the operations on these classes.
This is how negative numbers (for addition) and rational numbers (for multiplication) are built.
So if a theory is concerned with essentially monoids or categories, it likely implies they are not commutative, and these structures are rather esoteric.

--------

> Written with [StackEdit](https://stackedit.io/).