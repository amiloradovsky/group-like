
Group theory is a popular topic, so there are lots of information out there about it, but as a side effect of popularity, all these introductory courses are not always very consistent and coherent to each other: additional efforts are needed to figure out the architecture of the apparatus.
The same is true for, say, differential geometry, but I don't want to touch it right now.

The reason for popularity of group theory is because it is based on a very small set of axioms and very unexpected results are derived from them.
For instance, properties of different groups in a family may well be not so "even" and have special cases for some parameters (numbers) which are at least hard to predict, at most, no-one ever knows from where these exceptional cases arise.

Again, group theory is not only such a theory but possibly one of the oldest ones, after maybe only number theory.
Topology and mathematical logic, especially proof-type-category theory, for example, have as much or even bigger "ratio" of what may be derived to what has been assumed, but these theories are much younger and less known, therefore popular.

Also, group theory and, generally, abstract algebra, is also much less intuitive, therefore, hard to understand.
So, here I want to share some of my observations on this theory, give a short formulation of some of the formalism and augment it with some additional notes to make it look more sensible and less esoteric.

First, some lyrics.

[TOC]

Groups as an algebraic systems, constructivism
================================

So, a group is a simple algebraic system, defined by

- only one carrier, and three operations:
-  one binary (group operation, multiplication),
- one nullary (identity, neutral element, unit)
- one unary (inverse),

In constructive settings, it would also contain a proof of associativity, and of the properties of identity and inverse, but we'll ignore these constructive conditions for this time.

Alternatively, we might assume three binery operations instead:

- as earlier, one binary group operation,
- and two (binary) divisions, left and right.

And then prove that there exist at most only one neutral element and the only one inverse operation.
But this approach is not equivalent to the former due to, again, some constructive considerations, namely: unit may be build as an element, multiplied by it's inverse, it wouldn't depend on the element choosen, but first we'd have to choose one...: so it is not constructive.
This non-constructiveness may be overcome by means of pointedness: if in addition to divisions, there is a nullary operationo (an element) given, without any further properties, it might serve a proof that the carrier is not empty and therefore the unit really exists.

As an algebraic system, a group is not determined by the carrier alone, but also by the operations, and with different operations it forms different groups: so the common practice of denoting the carrier and the algebraic structure by the same symbol may be ambiguous.
Precisely logically, we should consider formal dependent pairs or tuples in order to describe an algebraic system, and use projections to refer to a component of it: carrier or an operation in case of a group.

The carriers themselves, of different algebraic systems in consideration, may be in some relation between one another: equal, (sub/super)set/-class of one another, be of different set-theoretic size etc.

It all is only supposed to show that despite the apparent simplicity of the group axioms, there is a lot of logical subtleties when dealing with the axioms, which should be treated carefully.

Group(iod)s and equivalence relations
================================
Groups correspond in a sense to _equivalence_ relations: so that

- the group operation corresponds to the transitivity property,
- the inverse operation corresponds to the symmetry property,
- the unit corresponds to the reflexivity property

of an equivalence relation.
Topology, more precisely, homotopy theory has much more to say on this.
Shortly, considering continuous loops in a topological space, based in a point of it, we may compose (concatenate) every such path with every other,
and this operation may be viewed as both:

- a group, of the based loops equipped with the concatenation operation, 
-  an equivalence relation, between the points of the space

Though in this case this relation is trivial, because it actually works for only pairs of the same points, this equivalence relation may be expanded onto the whole space, as path connectedness of a two points, but in this case we can no longer compose every path with every other.
This deficiency, however, may be resolved by considering more general structures than groups, called _groupoids_, which have almost all the same properties except that the group operation is defined not for every pair of elements.

As another generalization of the concept of a group, there is a concept of _monoid_, which doesn't require the existance of the inverse operation.
Similar generalization of the concept of a groupoid is the concept of a _category_, yet another now famous algebraic structure.
These algebraic structures correspond to _partial ordering_ relations in the same sence as above (no inverse - no symmetry).

Similarly, _semi-groups_ and _semi-categories_ are related to (mere) transitive relations (no identity - no reflexivity).

So we may gather these relations, between these associative algebraic structures and corresponding binary relations, in the following table.

| Feature | Totality | No totality | Relation | Feature
| -
| Inverse  | Group  | Groupoid | Equivalence | Symmetry
| Identity | Monoid | Category | Partial Ordering | Reflexivity
| Binary Operation | Semi-Group | Semi-Category | Transitive Relation | Transitivity

Finally, it may be seen that group(oid)s are some kind of refinement of the familiar equivalence relation, may be used to look into a deeper and subtler structure of equivalences.
Indeed, the notion of symmetry group is used to describe what it means for a (mathematical) construction to be symmetric, more precisely, under which exactly transformations the construction is still "the same" and which "different" states are ever there.

Also, a note on category-theoretic approach.
It pretends to be a radically different language, independend from many of the ambient mathematical traditions: diagrams instead of expressions, arrows may be interpreted not only as functions but also as relations held, much of the terminology is used in a slightly different sense than everywhere outside, etc.
So this is a culture pretty much disconnected from the rest of the enterprise.
I'm not sure whether it is good or not, after all, algebraic language is also far from being perfect, terminology is usually irregular, notation is often ambiguous.
But here I'm tending to prefer the more traditional approach and language, processing sequences of symbols structured with parentheses and sub- and superscripts... boring but clearer.


> Written with [StackEdit](https://stackedit.io/).