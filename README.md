Currently it's just a draft.
The intention was to build a verbose description of an elegant mathematical theory,
up to some quite non-obvious results (e.g. properties of S_6),
to subsequently formalize it in Coq, just as an exercise.